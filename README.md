# Tesis
Humanphobia is a game developed in Unreal Engine 4. The player controls a ghost in its attempt to scare a family out of its former home.

## Main features

- The player controls a ghost that floats and goes through walls. Allowing it to move freely around the house
- The members of the family maintain routines that make them move around the house as the day goes on. This brings a sandbox type of gameplay in which the player chooses how and when to scare the family members
- Each member of the family has a phobia, something that scares the more than anything else. The player must learn this if they want to scare them out of the house
- Sometimes, scaring someone goes wrong. The player may reveal it's presence and the family may start to be suspicious. In that case, they will call a ghost hunter that will act as an enemy of the player

## Art

Humanphobia was made from bundles provided by the Epic Games Store. Particularly, those provided by Giant Sparrow, from the game "What Remains of Edith Finch".

## Game

El [Documento de Diseño (GDD)](https://docs.google.com/document/d/1LR7J1odg5OV-52kb3ygIojYue3RBsns1KCMCfUfmAIM/edit?usp=sharing) completo en español se encuentra en este link.

The [Game Design Document](https://docs.google.com/document/d/15L4SwD2V_WGJ4wz6bJSetjN6xY6VNZhYfmWBpY0GKBA/edit?usp=sharing) can be found in this link.



Además es posible descargar la última versión del juego a través de este [link](https://pedroiriartew.itch.io/humanphobia-tesis).

You can also download the last version of the game through this [link](https://pedroiriartew.itch.io/humanphobia-tesis).
